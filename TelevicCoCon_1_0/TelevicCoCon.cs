﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.using System;

using System;
using System.Collections.Generic;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Net.Http;
using Newtonsoft.Json;

namespace TelevicCoCon_1_0
{
    public delegate void DelegateNoParametersEvent(EventArgs args);
    public delegate void DelegateUshort(ushort value);
    public delegate void DelegateInteger(int value);
    public delegate void DelegateIntegerArray(int[] value);
    public delegate void DelegateIntegerString(int intValue, SimplSharpString stringValue);
    public delegate void DelegateString(SimplSharpString value);
    public delegate void DelegateNoParameters();
    public delegate void DelegateIntegerInteger(int value1, int value2);

    public class TelevicCoCon
    {
        
        internal String iPaddress;
        internal int port;
        internal ConnectionManager serverConnection = null;
        internal string notificationID;
        private readonly CCriticalSection televicClientLock;
        internal bool isInitialized;
        internal event DelegateNoParametersEvent RecoverNotificationEvent;


        internal TelevicCoConSSharpComponent associatedSSharpComponent;   

        #region Initialization

        internal TelevicCoCon()
        {
            iPaddress = "";
            port = 8890;
            serverConnection = null;
            associatedSSharpComponent = null;
            notificationID = "";
            isInitialized = false;
            televicClientLock = new CCriticalSection();
            RecoverNotificationEvent += RecoverNotificationEventHandler;
        }

        internal void AssociateWithSSharpComponent(TelevicCoConSSharpComponent temp)
        {
            associatedSSharpComponent = temp;
        }

        internal void InitializeServer(string serverIpAddress, int serverPort)
        {
            iPaddress = serverIpAddress;
            port = serverPort;
            if(serverConnection == null)
                serverConnection = new ConnectionManager(iPaddress, port, ProcessTelevicUpdate, this);
        }

        internal void StartServerConnection()
        {
            if (serverConnection != null)
            {
                notificationID = serverConnection.StartNotificationConnection();

                if (associatedSSharpComponent != null && notificationID.Length != 0)
                {
                    if (serverConnection.RestartNotificationConnection(notificationID))
                    {
                        isInitialized = true;
                        associatedSSharpComponent.CompleteInitialization();
                    }
                    else
                    {
                        TriggerRecoverNotificationEvent();
                    }
                }
                else
                {
                    TriggerRecoverNotificationEvent();
                }
            }
        }

        internal void StopServerConnection()
        {
            if (serverConnection != null)
            {
                serverConnection.clearConnections();
            }
            isInitialized = false;
            if (associatedSSharpComponent != null)
                associatedSSharpComponent.ClearInitialization();
        }

        private void RecoverNotificationEventHandler(EventArgs args)
        {
            if (serverConnection != null)
            {
                serverConnection.RecoverNotificationConnection(notificationID);
            }
        }

        
        #endregion

        #region Server Commands

        internal void ProcessServerCommand(string command)
        {
            string response;
            try
            {
                televicClientLock.Enter();
                response = serverConnection.GetControlConnection().SendRequestToTelevic(command);
            }
            finally
            {
                televicClientLock.Leave();
            }
            if (response.Length > 4)
            {
                if (response.Substring(0, 2).Equals("\"{") && response.Substring(response.Length - 2, 2).Equals("}\""))
                    ProcessServerResponse(response);
                else
                    ErrorLog.Notice("httpresp: " + response);
                //if(response.Length > 0)
                //    ProcessServerResponse(response);
            }
        }

        internal void RequestMeetingList()
        {
            if(isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/GetMeetingsForToday");
        }

        internal void RequestAgendaInformation()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/GetAgendaItemInformationInRunningMeeting");
        }

        internal void SelectAgendaItem(int tempId)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetActiveAgendaItemById/?Id=" + tempId);
        }

        internal void SelectNextAgendaItem()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetActiveAgendaItemByDirection/?Direction=Next");
        }

        internal void SelectPreviousAgendaItem()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetActiveAgendaItemByDirection/?Direction=Previous");
        }

        internal void RequestDelegateInfo()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Delegate/GetDelegatesInCurrentMeeting");
        }

        internal void GetMicrophones()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Microphone/Get");
        }

        internal void GetMicrophoneStates()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Microphone/GetState");
        }

        internal void GetMicrophoneMode()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Microphone/GetMicrophoneMode");
        }

        internal void StartSelectedMeeting(int tempId)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetMeetingState/?State=Running&MeetingId=" + tempId);
        }

        internal void PauseSelectedMeeting(int tempId)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetMeetingState/?State=Paused&MeetingId=" + tempId);
        }

        internal void StopSelectedMeeting(int tempId)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Meeting_Agenda/SetMeetingState/?State=Ended&MeetingId=" + tempId);
        }

        internal void ChangeMicrophoneMode(int tempMode, int speakers, int requests)
        {
            if (isInitialized)
            {
                switch (tempMode)
                {
                    case (1):
                        ProcessServerCommand(@"/CoCon/Microphone/SetMicrophoneMode/?Mode=Operator");
                        break;
                    case (2):
                        var tempCmd = @"/CoCon/Microphone/SetMicrophoneMode/?Mode=Request&MaxNrActive=" + speakers + @"&MaxNrRequest=" + requests +@"&AllowRequest=true&AllowCancelRequest=true";
                        ProcessServerCommand(tempCmd);
                        break;
                    case (3):
                        ProcessServerCommand(@"/CoCon/Microphone/SetMicrophoneMode/?Mode=DirectSpeak");
                        break;
                }
            }
        }

        internal void ChangeMicrophoneMaxActive(string tempMode, int speakers, int requests)
        {
            if (isInitialized)
            {
                var tempCmd = @"/CoCon/Microphone/SetMicrophoneMode/?Mode=" + tempMode + @"&MaxNrActive=" + speakers + @"&MaxNrRequest=" + requests + @"&AllowRequest=true&AllowCancelRequest=true";
                ProcessServerCommand(tempCmd);
            }
        }

        internal void ChangeMicrophoneMaxRequest(string tempMode, int speakers, int requests)
        {
            if (!isInitialized) return;
            var tempCmd = @"/CoCon/Microphone/SetMicrophoneMode/?Mode=" + tempMode + @"&MaxNrActive=" + speakers + @"&MaxNrRequest=" + requests + @"&AllowRequest=true&AllowCancelRequest=true";
            ProcessServerCommand(tempCmd);
        }

        internal void TurnOffMcirophone(int tempSeatNr)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Microphone/SetState/?State=Off&SeatNr=" + tempSeatNr);
        }

        internal void StartVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Start");
        }

        internal void ResumeVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Resume");
        }

        internal void RestartVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Restart");
        }

        internal void PauseVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Pause");
        }

        internal void StopVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Stop");
        }

        internal void ClearVoting()
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/SetVotingState/?State=Clear");
        }

        internal void AddVoteWithTemplate(string temp)
        {
            if (isInitialized)
                ProcessServerCommand(@"/CoCon/Voting/AddInstantVote/?VotingTemplate=" + temp);
        }

        #endregion

        #region Server Responses

        private void ProcessTelevicUpdate(byte[] httpResponse, HTTP_CALLBACK_ERROR err)
        {
           // ErrorLog.Error("ProcessTelevicUpdate Callback started : " + err.ToString());

            if (err == HTTP_CALLBACK_ERROR.COMPLETED)
            {
                try
                {
                    if (httpResponse != null && httpResponse.Length > 0)
                    {
                        var tempString = Encoding.UTF8.GetString(httpResponse, 0, httpResponse.Length);

                        if (tempString.Substring(0, 2).Equals("\"{") && tempString.Substring(tempString.Length - 2, 2).Equals("}\""))
                        {
                            if (tempString.Equals("\"{\\\"Notification\\\" : \\\"Error 400\\\"}\""))
                            {
                                TriggerRecoverNotificationEvent();
                            }
                            else
                            {
                                ProcessServerUpdate(tempString);
                            }
                        }
                        else
                            ErrorLog.Notice("httpresp: " + tempString);
                    }
                }
                catch (Exception)
                {
                    ErrorLog.Error("Exception occured when receiving notification data");
                    TriggerRecoverNotificationEvent();
                }
                if (!serverConnection.RestartNotificationConnection(notificationID))
                    TriggerRecoverNotificationEvent();
            }
            else
            {
                ErrorLog.Error("Error occured in notification communication");
                TriggerRecoverNotificationEvent();
            }
        }

        internal void TriggerRecoverNotificationEvent()
        {
            if (RecoverNotificationEvent != null)
                RecoverNotificationEvent(new EventArgs());
        }

        internal void ProcessServerUpdate(string update)
        {
            TelevicCoConUpdateRoot parsedResponse = null;

            update = update.Replace("\\\"", "\"");
            update = update.Replace(@"\\", @"\");
            update = update.Replace("\\r\\n", "\\r");
            update = update.Trim('"');
            try
            {
                parsedResponse = AnalyzeReadUpdateData(update);
            }
            catch (Exception e)
            {
                ErrorLog.Error(update);
                ErrorLog.Exception(e.Message, e);
            }

            if (associatedSSharpComponent != null && parsedResponse != null)
            {
                if (parsedResponse.microphoneMode != null)
                {
                    associatedSSharpComponent.PopulateMicrophoneMode(parsedResponse.microphoneMode);
                    return;
                }

                if (parsedResponse.microphoneState != null)
                {
                    associatedSSharpComponent.PopulateMicrophoneState(parsedResponse.microphoneState);
                    return;
                }

                if (parsedResponse.meetingTitleUpdate != null)
                {
                    associatedSSharpComponent.TriggerActiveMeetingTitleUpdate(parsedResponse.meetingTitleUpdate.title);
                    return;
                }

                if (parsedResponse.activeAgendaItem != null)
                {
                    associatedSSharpComponent.TriggerActiveAgendaItemUpdate(parsedResponse.activeAgendaItem);
                    return;
                }

                if (parsedResponse.meetingState != null)
                {
                    associatedSSharpComponent.TriggerActiveMeetingStateUpdate(parsedResponse.meetingState);
                    return;
                }

                if (parsedResponse.meetingTimer != null)
                {
                    associatedSSharpComponent.TriggerActiveMeetingTimerUpdate(parsedResponse.meetingTimer.timer);
                    return;
                }

                if (parsedResponse.meetingTimerState != null)
                {
                    associatedSSharpComponent.TriggerActiveMeetingTimerStateUpdate(parsedResponse.meetingTimerState);
                }

                if (parsedResponse.activeAgendaItemTimer != null)
                {
                    associatedSSharpComponent.TriggerActiveAgendaItemTimerUpdate(parsedResponse.activeAgendaItemTimer.timer);
                }

                if (parsedResponse.activeAgendaItemTimerState != null)
                {
                    associatedSSharpComponent.TriggerActiveAgendaItemTimerStateUpdate(parsedResponse.activeAgendaItemTimerState);
                }

                if (parsedResponse.meetingUserInformatationUpdate != null)
                {
                    associatedSSharpComponent.ProcessDelegateInformationUpdate(parsedResponse.meetingUserInformatationUpdate);
                    return;
                }

                if (parsedResponse.badgeEventUpdate != null)
                {
                    associatedSSharpComponent.ProcessBadgeEventUpdate(parsedResponse.badgeEventUpdate);
                    return;
                }

                if (parsedResponse.delegateOnSeatUpdate != null)
                {
                    associatedSSharpComponent.ProcessDelegateOnSeatUpdate(parsedResponse.delegateOnSeatUpdate);
                    return;
                }

                if (parsedResponse.votingState != null)
                {
                    associatedSSharpComponent.TriggerVotingStateUpdate(parsedResponse.votingState);
                    return;
                }

                if (parsedResponse.votingOutcomeUpdate != null)
                {
                    associatedSSharpComponent.TriggerVotingOucomUpdate(parsedResponse.votingOutcomeUpdate);
                    return;
                }

                if (parsedResponse.votingResultSummary != null)
                {
                    associatedSSharpComponent.TriggerVotingResultSummaryUpdate(parsedResponse.votingResultSummary);
                    return;
                }

                if (parsedResponse.votingTimer != null)
                {
                    associatedSSharpComponent.TriggerVotingTimerUpdate(parsedResponse.votingTimer.votingTimer);
                    return;
                }

                if (parsedResponse.votingTimerState != null)
                {
                    associatedSSharpComponent.TriggerVotingTimerStateUpdate(parsedResponse.votingTimerState);
                }
            }
        }

        internal TelevicCoConUpdateRoot AnalyzeReadUpdateData(string response)
        {
            var result = JsonConvert.DeserializeObject<TelevicCoConUpdateRoot>(response);
            return result;
        }

        internal void ProcessServerResponse(string response)
        {
            TelevicCoConRoot parsedResponse = null;

            response = response.Replace("\\\"", "\"");
            response = response.Replace(@"\\", @"\");
            response = response.Replace("\\r\\n", "\\r");
            response = response.Trim('"');
            try
            {
                parsedResponse = AnalyzeReadData(response);
            }
            catch (Exception e)
            {
                ErrorLog.Exception(e.Message, e);
            }

            if (associatedSSharpComponent != null && parsedResponse != null)
            {
                if (parsedResponse.meetingResponse != null)
                {
                    associatedSSharpComponent.PopulateMeetingList(parsedResponse.meetingResponse);
                    return;
                }

                if (parsedResponse.delegateInCurrentMeetingsummary != null)
                {
                    associatedSSharpComponent.ProcessDelegateInformationUpdate(parsedResponse.delegateInCurrentMeetingsummary);
                    return;
                }

                if (parsedResponse.microphoneSummary != null)
                {
                    associatedSSharpComponent.PopulateMicrophoneSummary(parsedResponse.microphoneSummary);
                    return;
                }

                if (parsedResponse.microphoneModeSummary != null)
                {
                    associatedSSharpComponent.PopulateMicrophoneMode(parsedResponse.microphoneModeSummary.microphoneMode);
                    return;
                }

                if (parsedResponse.microphoneStateSummary != null)
                {
                    associatedSSharpComponent.PopulateMicrophoneState(parsedResponse.microphoneStateSummary.microphoneState);
                    return;
                }

                if (parsedResponse.agendatItemsSummary != null)
                {
                    associatedSSharpComponent.PopulateAgendaItemList(parsedResponse.agendatItemsSummary);
                }
            }
        }

        internal TelevicCoConRoot AnalyzeReadData(string response)
        {
            var result = JsonConvert.DeserializeObject<TelevicCoConRoot>(response);
            return result;
        }

        #endregion

    }


    #region JSON updates

    public class TelevicCoConUpdateRoot
    {
        [JsonProperty(PropertyName = "MeetingTimerEvent")]
        public MeetingTimer meetingTimer { get; set; }

        [JsonProperty(PropertyName = "MeetingTimerPauseResumed")]
        public TimerState meetingTimerState { get; set; }

        [JsonProperty(PropertyName = "ActiveAgendaItemTimerEvent")]
        public ActiveAgendaItemTimer activeAgendaItemTimer { get; set; }

        [JsonProperty(PropertyName = "ActiveAgendaItemTimerPauseResumed")]
        public TimerState activeAgendaItemTimerState { get; set; }

        [JsonProperty(PropertyName = "MicrophoneState")]
        public MicrophoneState microphoneState { get; set; }

        [JsonProperty(PropertyName = "MicrophoneMode")]
        public MicrophoneMode microphoneMode { get; set; }

        [JsonProperty(PropertyName = "MeetingTitleChanged")]
        public MeetingTitleUpdate meetingTitleUpdate { get; set; }

        [JsonProperty(PropertyName = "MeetingStatus")]
        public MeetingStateUpdate meetingState { get; set; }

        [JsonProperty(PropertyName = "Agenda_ItemChanged")]
        public ActiveAgendaItem activeAgendaItem { get; set; }

        [JsonProperty(PropertyName = "MeetingStartsUserInformation")]
        public MeetingUserInformatationUpdate meetingUserInformatationUpdate { get; set; }

        [JsonProperty(PropertyName = "VotingState")]
        public VotingState votingState { get; set; }

        [JsonProperty(PropertyName = "VotingOutcome")]
        public VotingOutcomeUpdate votingOutcomeUpdate { get; set; }

        [JsonProperty(PropertyName = "GeneralVotingResults")]
        public VotingResultSummary votingResultSummary { get; set; }

        [JsonProperty(PropertyName = "VotingTimerEvent")]
        public VotingTimer votingTimer { get; set; }

        [JsonProperty(PropertyName = "VotingTimerPauseResumed")]
        public TimerState votingTimerState { get; set; }

        [JsonProperty(PropertyName = "BadgeEvent")]
        public BadgeEventUpdate badgeEventUpdate { get; set; }

        [JsonProperty(PropertyName = "DelegateOnSeat")]
        public DelegateOnSeatUpdate delegateOnSeatUpdate { get; set; }
    }

    public class MeetingTimer
    {
        [JsonProperty(PropertyName = "MeetingTimer")]
        public BaseObjectTimer timer { get; set; }
    }

    public class ActiveAgendaItemTimer
    {
        [JsonProperty(PropertyName = "ActiveAgendaItemTimer")]
        public BaseObjectTimer timer { get; set; }
    }

    public class VotingTimer
    {
        [JsonProperty(PropertyName = "VotingTimer")]
        public BaseObjectTimer votingTimer { get; set; }
    }

    public class TimerState
    {
        [JsonProperty(PropertyName = "Paused")]
        public bool paused { get; set; }

        [JsonProperty(PropertyName = "VotingTimer")]
        public BaseObjectTimer votingTimer { get; set; }
    }

    public class MeetingTitleUpdate
    {
        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }
    }

    public class MeetingStateUpdate
    {
        [JsonProperty(PropertyName = "MeetingId")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string state { get; set; }
    }

    public class MeetingUserInformatationUpdate
    {
        [JsonProperty(PropertyName = "Users")]
        public ExtendedTelevicDelegate[] users { get; set; }
    }

    public class BadgeEventUpdate
    {
        [JsonProperty(PropertyName = "SeatNr")]
        public int seatNr { get; set; }

        [JsonProperty(PropertyName = "Delegate")]
        public TelevicDelegate user { get; set; }

        [JsonProperty(PropertyName = "BadgeInserted")]
        public bool badgeInserted { get; set; }
    }

    public class DelegateOnSeatUpdate
    {
        [JsonProperty(PropertyName = "SeatNr")]
        public int seatNr { get; set; }

        [JsonProperty(PropertyName = "Delegate")]
        public TelevicDelegate user { get; set; }

        [JsonProperty(PropertyName = "OnSeat")]
        public bool onSeat { get; set; }
    }

    public class ActiveAgendaItem
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string description { get; set; }

        [JsonProperty(PropertyName = "Type")]
        public string type { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string state { get; set; }
    }

    public class VotingState
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string state { get; set; }

       /* [JsonProperty(PropertyName = "VotingTemplate")]
        public string votingTemplate { get; set; }*/
    }

    public class VotingOutcomeUpdate
    {
        [JsonProperty(PropertyName = "OutCome")]
        public string outCome { get; set; }
    }

    #endregion

    #region JSON Command responses

    public class IntConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value != null ? (int)value : 0);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value == null ? 0 : Convert.ToInt32(reader.Value);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(int);
        }
    }

    public class TelevicCoConRoot
    {
        [JsonProperty(PropertyName = "GetMeetings")]
        public MeetingSummary meetingResponse { get; set; }

        [JsonProperty(PropertyName = "Get")]
        public MicrophoneSummary microphoneSummary { get; set; }
        
        [JsonProperty(PropertyName = "GetState")]
        public MicrophoneStateSummary microphoneStateSummary { get; set; }

        [JsonProperty(PropertyName = "GetMicrophoneMode")]
        public MicrophoneModeSummary microphoneModeSummary{ get; set; }

        [JsonProperty(PropertyName = "GetAgendaItemInformationInRunningMeeting")]
        public AgendatItemsSummary agendatItemsSummary { get; set; }
        
        [JsonProperty(PropertyName = "GetDelegatesInCurrentMeeting")]
        public DelegateInCurrentMeetingsummary delegateInCurrentMeetingsummary { get; set; }
       
        /* [JsonProperty(PropertyName = "GetAllTimers")]
        public jsonResponse response { get; set; }

        */

    }

    public class MeetingSummary
    {
        [JsonProperty(PropertyName = "Meetings")]
        public Meeting[] meetingList { get; set; }

    }

    public class AgendatItemsSummary
    {
        [JsonProperty(PropertyName = "AgendaItems")]
        public AgendaItem[] agendaItemList { get; set; }
    }

    public class DelegateInCurrentMeetingsummary
    {
        [JsonProperty(PropertyName = "IsMeetingRunning")]
        public bool isMeetingRunning { get; set; }

        [JsonProperty(PropertyName = "Delegates")]
        public TelevicDelegate[] delegates { get; set; }
    }

    public class MicrophoneSummary
    {
        [JsonProperty(PropertyName = "MicrophoneMode")]
        public MicrophoneMode microphoneMode { get; set; }

        [JsonProperty(PropertyName = "State")]
        public MicrophoneState state { get; set; }

    }

    public class MicrophoneModeSummary
    {
        [JsonProperty(PropertyName = "MicrophoneMode")]
        public MicrophoneMode microphoneMode { get; set; }
    }

    public class MicrophoneStateSummary
    {
        [JsonProperty(PropertyName = "State")]
        public MicrophoneState microphoneState { get; set; }
    }

    #endregion

    #region Televic JSON Sturctures

    public class MicrophoneMode
    {
        [JsonProperty(PropertyName = "Mode")]
        public string mode { get; set; }

        [JsonProperty(PropertyName = "MaxNrActive")]
        public int maxNrActive { get; set; }

        [JsonProperty(PropertyName = "MaxNrRequest")]
        public int maxNrRequest { get; set; }

        [JsonProperty(PropertyName = "AllowRequest")]
        public bool allowRequest { get; set; }

        [JsonProperty(PropertyName = "AllowCancelRequest")]
        public bool allowCancelRequest { get; set; }

        [JsonProperty(PropertyName = "FIFO")]
        public bool fIFO { get; set; }
    }

    public class MicrophoneState
    {
        [JsonProperty(PropertyName = "Speakers")]
        public int[] speakers { get; set; }

        [JsonProperty(PropertyName = "Requests")]
        public int[] requests { get; set; }
    }

    public class AuthorityAssigned
    {
        [JsonProperty(PropertyName = "Present")]
        public int present { get; set; }

        [JsonProperty(PropertyName = "Voted")]
        public int voted { get; set; }

        [JsonProperty(PropertyName = "Register")]
        public int register { get; set; }
    }

    public class Meeting
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string description { get; set; }

        [JsonProperty(PropertyName = "StartTime")]
        public string startTime { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string state { get; set; }
    }

    public class VotingCountWeight
    {
        [JsonProperty(PropertyName = "Count")]
        public int count { get; set; }

        [JsonProperty(PropertyName = "Weight")]
        public double weight { get; set; }
    }

    public class VotingOption
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "Color")]
        public string color { get; set; }
    }

    public class VotingOptionDetail
    {
        [JsonProperty(PropertyName = "Option")]
        public VotingOption option { get; set; }

        [JsonProperty(PropertyName = "OptionVoted")]
        public VotingCountWeight optionVoted { get; set; }
    }

    public class VotingResults
    {
        [JsonProperty(PropertyName = "Total")]
        public VotingCountWeight total { get; set; }

        [JsonProperty(PropertyName = "Voted")]
        public VotingCountWeight voted { get; set; }

        [JsonProperty(PropertyName = "NotVoted")]
        public VotingCountWeight notVoted { get; set; }

        [JsonProperty(PropertyName = "AuthorityAssigned")]
        public AuthorityAssigned authorityAssigned { get; set; }
    }

    public class VotingResultSummary
    {
        [JsonProperty(PropertyName = "VotingResults")]
        public VotingResults votingResults { get; set; }
    }

    public class Timer
    {
        [JsonProperty(PropertyName = "TotalTime")]
        public string totalTime { get; set; }

        [JsonProperty(PropertyName = "TimeUsed")]
        public string timeUsed { get; set; }

        [JsonProperty(PropertyName = "CountingDown")]
        public bool countingDown { get; set; }

        [JsonProperty(PropertyName = "WarningTime")]
        public string warningTime { get; set; }
    }

    public class BaseObjectTimer
    {
        [JsonProperty(PropertyName = "Timer")]
        public Timer timer { get; set; }
    }

    public class DelegateTimer : BaseObjectTimer
    {
        [JsonProperty(PropertyName = "DelegateId")]
        public int delegateId { get; set; }
    }

    public class GroupTimer : BaseObjectTimer
    {
        [JsonProperty(PropertyName = "GroupId")]
        public int groupId { get; set; }
    }

    public class SeatTimer : BaseObjectTimer
    {
        [JsonProperty(PropertyName = "SeatNr")]
        public int seatNr { get; set; }
    }

    public class ExtendedTelevicDelegate
    {
        [JsonProperty(PropertyName = "Id")]
        [JsonConverter(typeof(IntConverter))]
        public int id { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string firstName { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "Street")]
        public string street { get; set; }

        [JsonProperty(PropertyName = "StreetNumber")]
        [JsonConverter(typeof(IntConverter))]
        public int streetNumber { get; set; }

        [JsonProperty(PropertyName = "PostCode")]
        public string postCode { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string city { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string country { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "BirthDate")]
        public string birthDate { get; set; }

        [JsonProperty(PropertyName = "District")]
        public string district { get; set; }

        [JsonProperty(PropertyName = "Biography")]
        public string biography { get; set; }

        [JsonProperty(PropertyName = "Groups")]
        public Group[] groups { get; set; }

        [JsonProperty(PropertyName = "SeatNumber")]
        [JsonConverter(typeof(IntConverter))]
        public int seatNumber { get; set; }
    }

    public class TelevicDelegate
    {
        [JsonProperty(PropertyName = "Id")]
        [JsonConverter(typeof(IntConverter))]
        public int id { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string firstName { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "Street")]
        public string street { get; set; }

        [JsonProperty(PropertyName = "StreetNumber")]
        [JsonConverter(typeof(IntConverter))]
        public int streetNumber { get; set; }

        [JsonProperty(PropertyName = "PostCode")]
        public string postCode { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string city { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string country { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "BirthDate")]
        public string birthDate { get; set; }

        [JsonProperty(PropertyName = "District")]
        public string district { get; set; }

        [JsonProperty(PropertyName = "Biography")]
        public string biography { get; set; }

        [JsonProperty(PropertyName = "Groups")]
        public Group[] groups { get; set; }
    }

    public class Group
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string name { get; set; }
    }

    public class AgendaItem
    {
        [JsonProperty(PropertyName = "Id")]
        public int id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string title { get; set; }

        [JsonProperty(PropertyName = "Description")]
        public string description { get; set; }

        [JsonProperty(PropertyName = "Type")]
        public string type { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string state { get; set; }

        [JsonProperty(PropertyName = "Children")]
        public List<AgendaItem> children { get; set; }
    }

    public class VotingAgendaItem : AgendaItem
    {
        [JsonProperty(PropertyName = "VotingOptions")]
        public List<VotingOption> votingOptions { get; set; }
    }

    public class LecturerAgendaItem
    {
        [JsonProperty(PropertyName = "Lectures")]
        public List<TelevicDelegate> lectures { get; set; }
    }
    #endregion
}