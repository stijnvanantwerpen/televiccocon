﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Net.Http;

namespace TelevicCoCon_1_0
{
    public class TelevicClient
    {
        private readonly string _ip;
        private readonly int _port;
        
        private readonly HttpClientRequest _request;
        private readonly HttpClient _client;
        private HttpClientResponse _response;

        public bool KeepAlive
        {
            get
            {
                return _client.KeepAlive;
            }
            set
            {
                _client.KeepAlive = value;
            }
        }

        public TelevicClient()
        {
            _request = new HttpClientRequest();
            _request.Encoding = Encoding.UTF8;
            _response = null;
            
            _client = new HttpClient();
            _port = 8890;
            _ip = "127.0.0.1";
           
        }

        public TelevicClient(String ip, int port)
        {
            _request = new HttpClientRequest();
            _request.Encoding = Encoding.UTF8;
            _response = null;
            _client = new HttpClient();
            _port = port;
            _ip = ip;
        }

        public string SendRequestToTelevic(string args)
        {
            var url = "http://" + _ip + ":" + _port + args;
            
            _request.Url.Parse(url);
            _request.RequestType = RequestType.Get;

            try
            {
                _response = _client.Dispatch(_request);

                if (_response != null)
                {
                    _response.Encoding = Encoding.UTF8;

                    return _response.ContentString;
                }
                return "";
            }
            catch (Exception e)
            {
                ErrorLog.Exception("Unable to send the command to the Televic server", e);
                AbortConnection();
            }

            return "";
        }

        public HttpClient.DISPATCHASYNC_ERROR SendAsyncRequestToTelevic(string args, HTTPClientBytesCallback callback)
        {
            var url = "http://" + _ip + ":" + _port + args;

            _request.Url.Parse(url);
            _request.RequestType = RequestType.Get;
            HttpClient.DISPATCHASYNC_ERROR error;
            try
            {
                error = _client.DispatchBytesAsync(_request, callback);
            }
            catch (Exception e)
            {
                ErrorLog.Exception("Unable to send the command to the Televic server", e);
                AbortConnection();
                throw;
            }

            return error;
        }

        public void AbortConnection()
        {
            try
            {
                _client.Abort();
            }
            catch(Exception e)
            {
                ErrorLog.Exception(e.Message, e);
            }
        }
    }    
}