﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Net.Http;
using Newtonsoft.Json;

namespace TelevicCoCon_1_0
{
    public class ConnectionManager
    {
        private readonly TelevicClient _notificationClient;
        private readonly TelevicClient _controlClient;
        private readonly HTTPClientBytesCallback _httpCallbackFunction;
        private CTimer _connectionTimeoutTimer;
        internal TelevicCoCon associatedTelevicCoCon;
        
        internal ConnectionManager(String ServerIP,int Port, HTTPClientBytesCallback callbackFunction, TelevicCoCon televicCoCon)
        {
            _notificationClient = new TelevicClient(ServerIP,Port);
            _controlClient = new TelevicClient(ServerIP,Port);
            _httpCallbackFunction = callbackFunction;
            associatedTelevicCoCon = televicCoCon;

            _controlClient.KeepAlive = false;
        }

        internal TelevicClient GetNotificationConnection()
        {
            return _notificationClient;
        }

        internal TelevicClient GetControlConnection()
        {
            return _controlClient;
        }

        internal TelevicCoConNotificationConnectionData AnalyzeReadData(string response)
        {
            var result = JsonConvert.DeserializeObject<TelevicCoConNotificationConnectionData>(response);
            return result;
        }


        internal string StartNotificationConnection()
        {
            var response = "";
            TelevicCoConNotificationConnectionData tempResponse = null;

            _notificationClient.KeepAlive = false;
            try
            {
                response = _notificationClient.SendRequestToTelevic(@"/CoCon/Connect");
                response = response.Replace(@"\", string.Empty);
                response = response.Trim('"');
            }
            catch(Exception)
            {
                ErrorLog.Error("Could start Notification Connection");

                if(associatedTelevicCoCon != null)
                    associatedTelevicCoCon.TriggerRecoverNotificationEvent();
            }
            response = response.Replace(@"\", string.Empty);
            response = response.Trim('"');

            if (response.Length > 0)
            {
                try
                {
                    tempResponse = AnalyzeReadData(response);
                }
                catch (Exception e)
                {
                    ErrorLog.Exception(e.Message, e);
                }

                if (tempResponse != null && tempResponse.success)
                {
                    return tempResponse.ConnectionID;
                }
            }
            return response;
        }

        internal bool RestartNotificationConnection(string notificationID)
        {
            var result = false;
            if (notificationID.Length > 0)
            {
                var error = _notificationClient.SendAsyncRequestToTelevic("/CoCon/Notification/id=" + notificationID, _httpCallbackFunction);
                if (error == HttpClient.DISPATCHASYNC_ERROR.PENDING)
                    result = true;
            }
            return result;
        }

        internal void RecoverNotificationConnection(string notificationID)
        {
            _connectionTimeoutTimer = new CTimer(connectionTimeoutTimerCallback, 65000);
            if (associatedTelevicCoCon != null)
            {
                associatedTelevicCoCon.isInitialized = false;
                if (associatedTelevicCoCon.associatedSSharpComponent != null)
                    associatedTelevicCoCon.associatedSSharpComponent.ClearInitialization();
            }
            
        }

        internal void connectionTimeoutTimerCallback(Object userSpecific)
        {
            _connectionTimeoutTimer.Dispose();
            
            var tempID = StartNotificationConnection();
            if (tempID.Length > 0)
            {
                associatedTelevicCoCon.notificationID = tempID;
                if (RestartNotificationConnection(tempID) && associatedTelevicCoCon != null)
                {
                    associatedTelevicCoCon.isInitialized = true;
                    if (associatedTelevicCoCon.associatedSSharpComponent != null)
                        associatedTelevicCoCon.associatedSSharpComponent.CompleteInitialization();
                }
            }
            else
            {
                if (associatedTelevicCoCon != null)
                    associatedTelevicCoCon.TriggerRecoverNotificationEvent();
            }
        }

        internal void clearConnections()
        {
            _notificationClient.AbortConnection();
            _controlClient.AbortConnection();
        }
    }

    public class TelevicCoConNotificationConnectionData
    {
        [JsonProperty(PropertyName = "Connect")]
        public bool success { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string ConnectionID { get; set; }
    }
}