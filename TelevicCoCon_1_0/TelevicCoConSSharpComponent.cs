﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Crestron.SimplSharp;

namespace TelevicCoCon_1_0
{
    public class TelevicCoConSSharpComponent
    {
        #region SIMPL+ Callback Function Delegates

        public DelegateNoParameters basicInitializationIsComplete { get; set; }
        public DelegateInteger initializationIsComplete { get; set; }

        public DelegateString updateActiveMeetingTitle { get; set; }
        public DelegateString updateActiveMeetingDescription { get; set; }
        public DelegateString updateActiveMeetingStartTime { get; set; }
        public DelegateString updateActiveMeetingState { get; set; }
        public DelegateString updateActiveMeetingTimeUsed { get; set; }
        public DelegateString updateActiveMeetingTotalTime { get; set; }
        public DelegateString updateActiveMeetingWarningTime { get; set; }
        public DelegateInteger updateActiveMeetingTimerPaused { get; set; }

        public DelegateString updateActiveAgendaItemTitle { get; set; }
        public DelegateString updateActiveAgendaItemDescription { get; set; }
        public DelegateString updateActiveAgendaItemType { get; set; }
        public DelegateString updateActiveAgendaItemState { get; set; }
        public DelegateString updateActiveAgendaItemTimeUsed { get; set; }
        public DelegateString updateActiveAgendaItemTotalTime { get; set; }
        public DelegateString updateActiveAgendaItemWarningTime { get; set; }
        public DelegateInteger updateActiveAgendaItemTimerPaused { get; set; }
        public DelegateInteger updateActiveAgendaItemIsVotingType { get; set; }

        public DelegateNoParameters updateMeetingIsSelected { get; set; }
        public DelegateString updateSelectedMeetingTitle { get; set; }
        public DelegateString updateSelectedMeetingDescription { get; set; }
        public DelegateString updateSelectedMeetingStartTime { get; set; }
        public DelegateString updateSelectedMeetingState { get; set; }
        
        public DelegateIntegerString updateMeetingListTitleByIndex { get; set; }
        public DelegateIntegerString updateMeetingListDescriptionByIndex { get; set; }
        public DelegateIntegerString updateMeetingListIconByIndex { get; set; }
        public DelegateIntegerString updateMeetingListStartTimeByIndex { get; set; }
        public DelegateIntegerString updateMeetingListStateByIndex { get; set; }
        public DelegateInteger UpdateMeetingListItemCount { get; set; }

        public DelegateIntegerString updateAgendaItemListTitleByIndex { get; set; }
        public DelegateIntegerString updateAgendaItemListDescriptionByIndex { get; set; }
        public DelegateIntegerString updateAgendaItemListIconByIndex { get; set; }
        public DelegateIntegerString updateAgendaItemListTypeByIndex { get; set; }
        public DelegateIntegerString updateAgendaItemListStateByIndex { get; set; }
        public DelegateInteger UpdateAgendaItemListItemCount { get; set; }

        public DelegateIntegerString updateDelegateListNameByIndex { get; set; }
        public DelegateInteger UpdateDelegateListItemCount { get; set; }

        public DelegateString updateSelectedDelegateFirstName { get; set; }
        public DelegateString updateSelectedDelegateName { get; set; }
        public DelegateString updateSelectedDelegateStreet { get; set; }
        public DelegateInteger updateSelectedDelegateStreetNumber { get; set; }
        public DelegateString updateSelectedDelegatePostCode { get; set; }
        public DelegateString updateSelectedDelegateCity { get; set; }
        public DelegateString updateSelectedDelegateCountry { get; set; }
        public DelegateString updateSelectedDelegateTitle { get; set; }
        public DelegateString updateSelectedDelegateBirthDate { get; set; }
        public DelegateString updateSelectedDelegateDistrict { get; set; }
        public DelegateString updateSelectedDelegateBiography { get; set; }

        public DelegateString updateMicrophoneMode { get; set; }
        public DelegateInteger updateMicrophoneMaxNrActive { get; set; }
        public DelegateInteger updateMicrophoneMaxNrRequest { get; set; }

        public DelegateIntegerString updateSpeakerListByIndex { get; set; }
        public DelegateInteger updateSpeakerListItemCount { get; set; }
        public DelegateIntegerString updateRequestListByIndex { get; set; }
        public DelegateInteger updateRequestListItemCount { get; set; }

        public DelegateString updateVotingStatus { get; set; }
        public DelegateString updateVotingUsedTemplate { get; set; }
        public DelegateString updateVotingTimeUsed { get; set; }
        public DelegateString updateVotingTotalTime { get; set; }
        public DelegateString updateVotingWarningTime { get; set; }
        public DelegateInteger updateVotingTimerPaused { get; set; }
        public DelegateString updateVotingOutcome { get; set; }
        public DelegateIntegerString UpdateVotingTotal { get; set; }
        public DelegateIntegerString UpdateVotingVoted { get; set; }
        public DelegateIntegerString UpdateVotingNonVoted { get; set; }
        public DelegateInteger UpdateVotingAuthrityPresent { get; set; }
        public DelegateInteger UpdateVotingAuthorityVoted { get; set; }
        public DelegateInteger UpdateVotingAuthorityRegistered { get; set; }


        #endregion 
        
        private readonly TelevicCoCon associatedTelevicModule;
        private Meeting[] meetingList;
        private Meeting selectedMeeting;
        private AgendaItem[] agendaItemList;
        private MicrophoneState microphoneState;
        private TelevicDelegate[] delegateList;
        private TelevicDelegate selectedDelegate;
        private readonly Dictionary<int, string> seatNrNameTable;
        private int acivteMeetingID;
        private bool isInitialized;
        private string votingStatus;
        private int maxSpeakers;
        private int maxRequests;
        private string micMode;
        readonly Dictionary<string, string> meetingStateIcons;
        readonly Dictionary<string, string> AgendaItemStateIcons;

        private readonly CCriticalSection meetingListLock;
        private readonly CCriticalSection selectedMeetingLock;
        private readonly CCriticalSection agendaItemListLock;
        private readonly CCriticalSection microphoneStateLock;
        private readonly CCriticalSection delegateListLock;
        private readonly CCriticalSection selectedDelegateLock;
        private readonly CCriticalSection seatNrNameTableLock;
        
        #region Initialization
              
        public TelevicCoConSSharpComponent() 
        {
            associatedTelevicModule = new TelevicCoCon();
            associatedTelevicModule.AssociateWithSSharpComponent(this);
            meetingList = null;
            selectedMeeting = null;
            agendaItemList = null;
            microphoneState = null;
            delegateList = null;
            seatNrNameTable = new Dictionary<int,string>();
            acivteMeetingID = 0;
            isInitialized = false;
            micMode = "";
            maxRequests = 0;
            maxSpeakers = 0;
            votingStatus = "";
            meetingStateIcons = new Dictionary<string, string>();            
            AgendaItemStateIcons = new Dictionary<string,string>();

            meetingListLock = new CCriticalSection();
            selectedMeetingLock = new CCriticalSection();
            agendaItemListLock = new CCriticalSection();
            microphoneStateLock = new CCriticalSection();
            delegateListLock = new CCriticalSection();
            selectedDelegateLock = new CCriticalSection();
            seatNrNameTableLock = new CCriticalSection();
        }

        public void InitializeSettings(string serverIpAddress, int serverPort)
        {
            if (isInitialized) return;
            if (associatedTelevicModule != null)
                associatedTelevicModule.InitializeServer(serverIpAddress, serverPort);
            if (basicInitializationIsComplete != null)
                basicInitializationIsComplete();
        }

        public void SetStateIcons(string tempMeetingNew, string tempMeetingEditing, string tempMeetingStarted,
            string tempMeetingPaused, string tempMeetingEnded, string tempAgendaItemNotActive, string tempAgendaItemActive, string tempAgendaItemEnded)
        {
            meetingStateIcons.Add("New", tempMeetingNew);
            meetingStateIcons.Add("Editing", tempMeetingEditing);
            meetingStateIcons.Add("Running", tempMeetingStarted);
            meetingStateIcons.Add("Paused", tempMeetingPaused);
            meetingStateIcons.Add("Ended", tempMeetingEnded);
            AgendaItemStateIcons.Add("notstarted", tempAgendaItemNotActive);
            AgendaItemStateIcons.Add("active", tempAgendaItemActive);
            AgendaItemStateIcons.Add("ended", tempAgendaItemEnded);
        }

        public void BeginInitialization()
        {
            if(associatedTelevicModule != null)
                associatedTelevicModule.StartServerConnection();            
        }

        public void StopServerConnection()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.StopServerConnection();
        }

        internal void CompleteInitialization()
        {
            isInitialized = true;
            if (initializationIsComplete != null)
                initializationIsComplete(1);
        }
        internal void ClearInitialization()
        {
            isInitialized = false;
            if (initializationIsComplete != null)
                initializationIsComplete(0);
        }

        #endregion

        #region Meetings

        public void RequestMeetingList()
        {
            if(associatedTelevicModule != null)
                associatedTelevicModule.RequestMeetingList();
        }

         public void MeetingListItemSelected(int index)
        {
            if (meetingList != null)
            {
                try
                {
                    meetingListLock.Enter();
                    if (index < meetingList.Length)
                    {
                        selectedMeeting = meetingList[index];
                        PopulateSelectedMeetingInfo();
                    }
                }
                finally
                {
                    meetingListLock.Leave();
                }
            }
        }

        internal void PopulateSelectedMeetingInfo()
        {
            if (selectedMeeting != null)
            {
                try
                {
                    selectedMeetingLock.Enter();
                    if (updateSelectedMeetingTitle != null)
                        updateSelectedMeetingTitle(selectedMeeting.title);

                    if (updateSelectedMeetingDescription != null)
                        updateSelectedMeetingDescription(selectedMeeting.description);

                    if (updateSelectedMeetingStartTime != null)
                        updateSelectedMeetingStartTime(selectedMeeting.startTime);

                    if (updateSelectedMeetingState != null)
                        updateSelectedMeetingState(selectedMeeting.state);

                    if (updateMeetingIsSelected != null)
                        updateMeetingIsSelected();
                }
                finally
                {
                    selectedMeetingLock.Leave();
                }
            }
        }

        internal void UpdateSelectedMeetingInfo()
        {
            if (selectedMeeting != null)
            {
                try
                {
                    selectedMeetingLock.Enter();
                    if (updateSelectedMeetingTitle != null)
                        updateSelectedMeetingTitle(selectedMeeting.title);

                    if (updateSelectedMeetingDescription != null)
                        updateSelectedMeetingDescription(selectedMeeting.description);

                    if (updateSelectedMeetingStartTime != null)
                        updateSelectedMeetingStartTime(selectedMeeting.startTime);

                    if (updateSelectedMeetingState != null)
                        updateSelectedMeetingState(selectedMeeting.state);

                }
                finally
                {
                    selectedMeetingLock.Leave();
                }
            }
        }

        public void StartSelectedMeeting()
        {
            if (associatedTelevicModule != null && selectedMeeting != null)
            {
                try
                {
                    selectedMeetingLock.Enter();
                    associatedTelevicModule.StartSelectedMeeting(selectedMeeting.id);
                }
                finally
                {
                    selectedMeetingLock.Leave();
                }
            }
        }

        public void PauseSelectedMeeting()
        {
            if (associatedTelevicModule != null && selectedMeeting != null)
            {
                try
                {
                    selectedMeetingLock.Enter();
                    associatedTelevicModule.PauseSelectedMeeting(selectedMeeting.id);
                }
                finally
                {
                    selectedMeetingLock.Leave();
                }
            }
        }

        public void StopSelectedMeeting()
        {
            if (associatedTelevicModule != null && selectedMeeting != null)
            {
                try
                {
                    selectedMeetingLock.Enter();
                    associatedTelevicModule.StopSelectedMeeting(selectedMeeting.id);
                }
                finally
                {
                    selectedMeetingLock.Leave();
                }
            }
        }

        public void StartActiveMeeting()
        {
            if (associatedTelevicModule != null && acivteMeetingID != 0)
            {
                associatedTelevicModule.StartSelectedMeeting(acivteMeetingID);
            }

        }

        public void PauseActiveMeeting()
        {
            if (associatedTelevicModule != null && acivteMeetingID != 0)
            {
                associatedTelevicModule.PauseSelectedMeeting(acivteMeetingID);
            }

        }

        public void StopActiveMeeting()
        {
            if (associatedTelevicModule != null && acivteMeetingID != 0)
            {
                associatedTelevicModule.StopSelectedMeeting(acivteMeetingID);
            }

        }

        internal void PopulateMeetingList(MeetingSummary temp)
        {
            if(temp != null && temp.meetingList != null)
            {
                try
                {
                    meetingListLock.Enter();
                    meetingList = temp.meetingList;
                    
                    if (temp.meetingList.Length != 0)
                    {
                        for (var i = 0; i < meetingList.Length; i++)
                        {
                            if (updateMeetingListTitleByIndex != null)
                                updateMeetingListTitleByIndex(i, temp.meetingList[i].title);

                            if (updateMeetingListDescriptionByIndex != null)
                                updateMeetingListDescriptionByIndex(i, temp.meetingList[i].description);

                            if (updateMeetingListStartTimeByIndex != null)
                            {
                                var tempResult = "";

                                //int tempIndex;
                                var tempTime = temp.meetingList[i].startTime.Split(' ');
                                //if (tempIndex > 0 )
                                //{
                                    //tempTime = temp.meetingList[i].startTime.Substring(tempIndex + 1, temp.meetingList[i].startTime.Length - tempIndex - 1);.
                                if (tempTime.Length == 6)
                                    tempResult = tempTime[4] + " " + tempTime[5];
                                else if (tempTime.Length == 2)
                                    tempResult = tempTime[1];

                                updateMeetingListStartTimeByIndex(i, tempResult);
                                //}
                            }

                            if (updateMeetingListStateByIndex != null)
                                updateMeetingListStateByIndex(i, temp.meetingList[i].state);

                            if (updateMeetingListIconByIndex != null)
                                updateMeetingListIconByIndex(i, meetingStateIcons[temp.meetingList[i].state]);

                            if (temp.meetingList[i].state.Equals("Running") || temp.meetingList[i].state.Equals("Paused"))
                            {
                                acivteMeetingID = temp.meetingList[i].id;
                                TriggerActiveMeetingUpdate(temp.meetingList[i].title, temp.meetingList[i].description, temp.meetingList[i].startTime, temp.meetingList[i].state);
                            }

                        }
                    }
                    if (UpdateMeetingListItemCount != null)
                        UpdateMeetingListItemCount(temp.meetingList.Length);
                }
                finally
                {
                    meetingListLock.Leave();
                }
                
            }
        }

        internal void TriggerActiveMeetingTitleUpdate(string tempTitle)
        {
            if (tempTitle != null)
            {
                if (updateActiveMeetingTitle != null)
                    updateActiveMeetingTitle(tempTitle);
                try
                {
                    meetingListLock.Enter();
                    foreach (var t in meetingList.Where(t => t.id == acivteMeetingID))
                    {
                        t.title = tempTitle;
                    }
                }
                finally
                {
                    meetingListLock.Leave();
                }

            }
        }

        internal void TriggerActiveMeetingStateUpdate(MeetingStateUpdate temp)
        {
            if (temp != null)
            {
                if (selectedMeeting != null)
                {
                    try
                    {
                        selectedMeetingLock.Enter();
                        if (temp.id == selectedMeeting.id)
                        {
                            selectedMeeting.state = temp.state;
                            UpdateSelectedMeetingInfo();
                        }

                    }
                    finally
                    {
                        selectedMeetingLock.Leave();
                    }
                }
                if (meetingList != null)
                {
                    try
                    {
                        meetingListLock.Enter();
                        for (var i = 0; i < meetingList.Length; i++)
                        {
                            if (meetingList[i].id == temp.id)
                            {
                                meetingList[i].state = temp.state;
                                if (updateMeetingListStateByIndex != null)
                                    updateMeetingListStateByIndex(i, temp.state);

                                if (updateMeetingListIconByIndex != null)
                                    updateMeetingListIconByIndex(i, meetingStateIcons[temp.state]);

                                if (temp.id == acivteMeetingID)
                                {
                                    TriggerActiveMeetingUpdate(meetingList[i].title, meetingList[i].description, meetingList[i].startTime, temp.state);
                                    if (temp.state.Equals("Ended"))
                                    {
                                        ClearMeetingData();
                                    }

                                }
                                else if (temp.state.Equals("Running"))
                                {
                                    acivteMeetingID = temp.id;
                                    TriggerActiveMeetingUpdate(meetingList[i].title, meetingList[i].description, meetingList[i].startTime, temp.state);
                                }

                            }
                        }
                    }
                    finally
                    {
                        meetingListLock.Leave();
                    }
                }
            }
        }

        

        internal void TriggerActiveMeetingUpdate(string tempTitle, string tempDescription, string tempStartTime, string tempState)
        {
            if (updateActiveMeetingTitle != null)
                updateActiveMeetingTitle(tempTitle);

            if (updateActiveMeetingDescription != null)
                updateActiveMeetingDescription(tempDescription);

            if (updateActiveMeetingStartTime != null)
                updateActiveMeetingStartTime(tempStartTime);

            if (updateActiveMeetingState != null)
                updateActiveMeetingState(tempState);
        }

        internal void TriggerActiveMeetingTimerUpdate(BaseObjectTimer temp)
        {
            if (temp != null && temp.timer != null)
            {
                if(updateActiveMeetingTimeUsed != null)
                    updateActiveMeetingTimeUsed(temp.timer.timeUsed);

                if (updateActiveMeetingTotalTime != null)
                    updateActiveMeetingTotalTime(temp.timer.totalTime);

                if (updateActiveMeetingWarningTime != null)
                    updateActiveMeetingWarningTime(temp.timer.warningTime);

            }
        }

        internal void TriggerActiveMeetingTimerStateUpdate(TimerState temp)
        {
            if (temp == null) return;
            if (updateActiveMeetingTimerPaused != null)
            {
                updateActiveMeetingTimerPaused(temp.paused ? 1 : 0);
            }

            TriggerActiveMeetingTimerUpdate(temp.votingTimer);
        }

        internal void ClearMeetingData()
        {
            ClearMicrophoneState();
            ClearAgenda();
            ClearDelegateData();
            ClearVotingData();
        }

        #endregion

        #region Agenda Items

        public void RequestAgendaInfo()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.RequestAgendaInformation();
        }

        public void SelectNextAgendaTopic()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.SelectNextAgendaItem();
        }

        public void SelectPreviousAgendaTopic()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.SelectPreviousAgendaItem();
        }

        public void AgendatItemListItemSelected(int index)
        {
            if (agendaItemList != null && index < agendaItemList.Length)
            {
                try
                {
                    agendaItemListLock.Enter();
                    if(index < agendaItemList.Length)
                        associatedTelevicModule.SelectAgendaItem(agendaItemList[index].id);
                }
                finally
                {
                    agendaItemListLock.Leave();
                }
            }
        }

        internal AgendaItem[] FlattenAgendaItems(AgendaItem[] temp)
        {
            return FlattenAgendaItemsList(new List<AgendaItem>(temp)).ToArray();
        }

        internal List<AgendaItem> FlattenAgendaItemsList(List<AgendaItem> temp)
        {
            var result = new List<AgendaItem>();
            if (temp != null)
            {
                if (temp.Count > 0)
                {
                    foreach(var tempItem in temp)
                    {
                        result.Add(tempItem);
                        if (tempItem.children != null)
                        {
                            result.AddRange(FlattenAgendaItemsList(tempItem.children));
                        }
                    }
                }
                return result;
            }
            return new List<AgendaItem>();
        }

        internal void PopulateAgendaItemList(AgendatItemsSummary temp)
        {
            if (temp != null && temp.agendaItemList != null)
            {
                try
                {
                    agendaItemListLock.Enter();
                    agendaItemList = FlattenAgendaItems(temp.agendaItemList);
                

                    if (agendaItemList.Length != 0)
                    {
                        for (var i = 0; i < agendaItemList.Length; i++)
                        {
                            if (updateAgendaItemListTitleByIndex != null)
                                updateAgendaItemListTitleByIndex(i, agendaItemList[i].title);

                            if (updateAgendaItemListDescriptionByIndex != null)
                                updateAgendaItemListDescriptionByIndex(i, agendaItemList[i].description);

                            if (updateAgendaItemListTypeByIndex != null)
                                updateAgendaItemListTypeByIndex(i, agendaItemList[i].type);

                            if (updateAgendaItemListStateByIndex != null)
                                updateAgendaItemListStateByIndex(i, agendaItemList[i].state);

                            if (updateAgendaItemListIconByIndex != null)
                                updateAgendaItemListIconByIndex(i, AgendaItemStateIcons[agendaItemList[i].state]);

                            if (agendaItemList[i].state.Equals("active"))
                            {
                                if (updateActiveAgendaItemTitle != null)
                                    updateActiveAgendaItemTitle(agendaItemList[i].title);

                                if (updateActiveAgendaItemDescription != null)
                                    updateActiveAgendaItemDescription(agendaItemList[i].description);

                                if (updateActiveAgendaItemType != null)
                                {
                                    updateActiveAgendaItemType(agendaItemList[i].type);

                                    updateActiveAgendaItemIsVotingType(agendaItemList[i].type == "VotingAgendaItem"
                                        ? 1: 0);
                                }

                                if (updateActiveAgendaItemState != null)
                                    updateActiveAgendaItemState(agendaItemList[i].state);
                            }

                        }
                    }

                    if (UpdateAgendaItemListItemCount != null)
                        UpdateAgendaItemListItemCount(agendaItemList.Length);

                }
                finally
                {
                    agendaItemListLock.Leave();
                }
            }
        }

        internal void TriggerActiveAgendaItemUpdate(ActiveAgendaItem temp)
        {
            var found = false;
            if (temp != null && agendaItemList != null)
            {
                if (updateActiveAgendaItemTitle != null)
                    updateActiveAgendaItemTitle(temp.title);

                if (updateActiveAgendaItemDescription != null)
                    updateActiveAgendaItemDescription(temp.description);

                if (updateActiveAgendaItemState != null)
                    updateActiveAgendaItemState(temp.state);
                try
                {
                    agendaItemListLock.Enter();
                    for (var i = 0; i < agendaItemList.Length; i++)
                    {
                        if (agendaItemList[i].id == temp.id)
                        {
                            found = true;
                            agendaItemList[i].title = temp.title;
                            if (updateAgendaItemListTitleByIndex != null)
                                updateAgendaItemListTitleByIndex(i, temp.title);

                            agendaItemList[i].description = temp.description;
                            if (updateAgendaItemListDescriptionByIndex != null)
                                updateAgendaItemListDescriptionByIndex(i, temp.description);

                            if (updateActiveAgendaItemType != null)
                                updateActiveAgendaItemType(agendaItemList[i].type);

                            if(updateActiveAgendaItemIsVotingType != null)
                            {
                                updateActiveAgendaItemIsVotingType(agendaItemList[i].type == "VotingAgendaItem" ? 1 : 0);
                            }
                            agendaItemList[i].state = temp.state;
                            if (updateAgendaItemListStateByIndex != null)
                                updateAgendaItemListStateByIndex(i, temp.state);
                        }
                    }
                }
                finally
                {
                    agendaItemListLock.Leave();
                }
                if (!found && associatedTelevicModule != null)
                    associatedTelevicModule.RequestAgendaInformation();

            }
        }

        internal void TriggerActiveAgendaItemTimerUpdate(BaseObjectTimer temp)
        {
            if (temp != null && temp.timer != null)
            {
                if(updateActiveAgendaItemTimeUsed != null)
                    updateActiveAgendaItemTimeUsed(temp.timer.timeUsed);

                if (updateActiveAgendaItemTotalTime != null)
                    updateActiveAgendaItemTotalTime(temp.timer.totalTime);

                if (updateActiveAgendaItemWarningTime != null)
                    updateActiveAgendaItemWarningTime(temp.timer.warningTime);

            }
        }

        internal void TriggerActiveAgendaItemTimerStateUpdate(TimerState temp)
        {
            if (temp != null)
            {
                if (updateActiveAgendaItemTimerPaused != null)
                {
                    updateActiveAgendaItemTimerPaused(temp.paused ? 1 : 0);
                }

                TriggerActiveAgendaItemTimerUpdate(temp.votingTimer);
            }
        }

        internal void ClearAgenda()
        {
            try
            {
                agendaItemListLock.Enter();
                agendaItemList = null;
            }
            finally
            {
                agendaItemListLock.Leave();
            }

            for (var i = 0; i < 50; i++)
            {
                if (updateAgendaItemListTitleByIndex != null)
                    updateAgendaItemListTitleByIndex(i, "");

                if (updateAgendaItemListDescriptionByIndex != null)
                    updateAgendaItemListDescriptionByIndex(i, "");

                if (updateAgendaItemListTypeByIndex != null)
                    updateAgendaItemListTypeByIndex(i, "");

                if (updateAgendaItemListStateByIndex != null)
                    updateAgendaItemListStateByIndex(i, "");

                if (updateAgendaItemListIconByIndex != null)
                    updateAgendaItemListIconByIndex(i, "");
            }

            if (UpdateAgendaItemListItemCount != null)
                UpdateAgendaItemListItemCount(0);


            if (updateActiveAgendaItemTitle != null)
                updateActiveAgendaItemTitle("");

            if (updateActiveAgendaItemDescription != null)
                updateActiveAgendaItemDescription("");

            if (updateActiveAgendaItemType != null)
                updateActiveAgendaItemType("");

            if (updateActiveAgendaItemIsVotingType != null)
                updateActiveAgendaItemIsVotingType(0);

            if (updateActiveAgendaItemState != null)
                updateActiveAgendaItemState("");

            if (updateActiveAgendaItemTimeUsed != null)
                updateActiveAgendaItemTimeUsed("");

            if (updateActiveAgendaItemTotalTime != null)
                updateActiveAgendaItemTotalTime("");

            if (updateActiveAgendaItemWarningTime != null)
                updateActiveAgendaItemWarningTime("");
        }

        #endregion

        #region Televic delegates

        public void RequestDelegateInfo()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.RequestDelegateInfo();
        }

        internal void ProcessDelegateInformationUpdate(MeetingUserInformatationUpdate temp)
        {
            if(temp.users != null)
            {
                CreateDelegateSeatTable(temp.users);
            }

        }

        internal void ProcessBadgeEventUpdate(BadgeEventUpdate temp)
        {
            if (temp != null)
            {
                UpdateDelegateSeatTable(temp);
            }
        }

        internal void ProcessDelegateOnSeatUpdate(DelegateOnSeatUpdate temp)
        {
            if (temp != null)
            {
                UpdateDelegateSeatTable(temp);
            }
        }

        internal void ProcessDelegateInformationUpdate(DelegateInCurrentMeetingsummary temp)
        {
            if (temp.delegates != null)
            {
                try
                {
                    delegateListLock.Enter();
                    delegateList = temp.delegates;
                }
                finally
                {
                    delegateListLock.Leave();
                }

                TriggerDelegateListUpdate(temp.delegates);
            }
        }

        internal void CreateDelegateSeatTable(ExtendedTelevicDelegate[] temp)
        {            
            if (temp != null)
            {
                try
                {
                    seatNrNameTableLock.Enter();
                    seatNrNameTable.Clear();
                    foreach (var t in temp.Where(t => t.seatNumber > 0))
                    {
                        seatNrNameTable.Add(t.seatNumber, t.firstName + " " + t.name);
                    }
                }
                finally
                {
                    seatNrNameTableLock.Leave();
                }
                UpdateMicrophoneState();
            }
        }

        internal void UpdateDelegateSeatTable(BadgeEventUpdate temp)
        {
            if (temp != null)
            {
                try
                {
                    seatNrNameTableLock.Enter();

                    if (temp.badgeInserted)
                    {
                        if (seatNrNameTable.ContainsKey(temp.seatNr))
                        {
                            seatNrNameTable[temp.seatNr] = temp.user.firstName + " " + temp.user.name;
                        }
                        else
                        {
                            seatNrNameTable.Add(temp.seatNr, temp.user.firstName + " " + temp.user.name);
                        }
                    }
                    else
                    {
                        if (seatNrNameTable.ContainsKey(temp.seatNr))
                        {
                            seatNrNameTable.Remove(temp.seatNr);
                        }
                    }

                }
                finally
                {
                    seatNrNameTableLock.Leave();
                }
                UpdateMicrophoneState();
            }
        }

        internal void UpdateDelegateSeatTable(DelegateOnSeatUpdate temp)
        {
            if (temp != null)
            {
                try
                {
                    seatNrNameTableLock.Enter();

                    if (temp.onSeat)
                    {
                        if (seatNrNameTable.ContainsKey(temp.seatNr))
                        {
                            seatNrNameTable[temp.seatNr] = temp.user.firstName + " " + temp.user.name;
                        }
                        else
                        {
                            seatNrNameTable.Add(temp.seatNr, temp.user.firstName + " " + temp.user.name);
                        }
                    }
                    else
                    {
                        if (seatNrNameTable.ContainsKey(temp.seatNr))
                        {
                            seatNrNameTable.Remove(temp.seatNr);
                        }
                    }

                }
                finally
                {
                    seatNrNameTableLock.Leave();
                }

                UpdateMicrophoneState();
            }
        }
        
        internal void TriggerDelegateListUpdate(TelevicDelegate[] temp)
        {
            if (temp != null)
            {
                if (updateDelegateListNameByIndex != null)
                {
                    for (var i = 0; i < temp.Length; i++)
                    {
                        updateDelegateListNameByIndex(i, temp[i].firstName + " " + temp[i].name);
                    }
                }

                if (UpdateDelegateListItemCount != null)
                {
                    UpdateDelegateListItemCount(temp.Length);
                }
            }
        }

        public void DelegateListItemSelected(int index)
        {
            if (delegateList != null)
            {
                try
                {
                    selectedDelegateLock.Enter();
                    if(index < delegateList.Length)
                    {
                        selectedDelegate = delegateList[index];
                        UpdateSelectedDelegateInformation();
                    }
                }
                finally
                {
                    selectedDelegateLock.Leave();
                }
            }
        }

        internal void UpdateSelectedDelegateInformation()
        {
            if (selectedDelegate != null)
            {
                try
                {
                    selectedDelegateLock.Enter();
                    if (updateSelectedDelegateFirstName != null && selectedDelegate.firstName != null)
                        updateSelectedDelegateFirstName(selectedDelegate.firstName);

                    if (updateSelectedDelegateName != null && selectedDelegate.name != null)
                        updateSelectedDelegateName(selectedDelegate.name);

                    if (updateSelectedDelegateStreet != null && selectedDelegate.street != null)
                        updateSelectedDelegateStreet(selectedDelegate.street);

                    if (updateSelectedDelegateStreetNumber != null)
                        updateSelectedDelegateStreetNumber(selectedDelegate.streetNumber);

                    if (updateSelectedDelegatePostCode != null && selectedDelegate.postCode != null)
                        updateSelectedDelegatePostCode(selectedDelegate.postCode);

                    if (updateSelectedDelegateCity != null && selectedDelegate.city != null)
                        updateSelectedDelegateCity(selectedDelegate.city);

                    if (updateSelectedDelegateCountry != null && selectedDelegate.country != null)
                        updateSelectedDelegateCountry(selectedDelegate.country);

                    if (updateSelectedDelegateTitle != null && selectedDelegate.title != null)
                        updateSelectedDelegateTitle(selectedDelegate.title);

                    if (updateSelectedDelegateBirthDate != null && selectedDelegate.birthDate != null)
                        updateSelectedDelegateBirthDate(selectedDelegate.birthDate);

                    if (updateSelectedDelegateDistrict != null && selectedDelegate.district != null)
                        updateSelectedDelegateDistrict(selectedDelegate.district);

                    if (updateSelectedDelegateBiography != null && selectedDelegate.biography != null)
                        updateSelectedDelegateBiography(selectedDelegate.biography);
                }
                finally
                {
                    selectedDelegateLock.Leave();
                }
            }
        }

        internal void ClearDelegateData()
        {
            try
            {
                selectedDelegateLock.Enter();
                selectedDelegate = null;
            }
            catch
            {
                selectedDelegateLock.Leave();
            }

            if (updateSelectedDelegateFirstName != null)
                updateSelectedDelegateFirstName("");

            if (updateSelectedDelegateName != null)
                updateSelectedDelegateName("");

            if (updateSelectedDelegateStreet != null)
                updateSelectedDelegateStreet("");

            if (updateSelectedDelegateStreetNumber != null)
                updateSelectedDelegateStreetNumber(0);

            if (updateSelectedDelegatePostCode != null)
                updateSelectedDelegatePostCode("");

            if (updateSelectedDelegateCity != null)
                updateSelectedDelegateCity("");

            if (updateSelectedDelegateCountry != null)
                updateSelectedDelegateCountry("");

            if (updateSelectedDelegateTitle != null)
                updateSelectedDelegateTitle("");

            if (updateSelectedDelegateBirthDate != null)
                updateSelectedDelegateBirthDate("");

            if (updateSelectedDelegateDistrict != null)
                updateSelectedDelegateDistrict("");

            if (updateSelectedDelegateBiography != null)
                updateSelectedDelegateBiography("");

            try
            {
                delegateListLock.Enter();
                delegateList = null;
            }
            finally
            {
                delegateListLock.Leave();
            }

            if (updateDelegateListNameByIndex != null)
            {
                for (var i = 0; i < 300; i++)
                {
                    updateDelegateListNameByIndex(i, "");
                }
            }

            if (UpdateDelegateListItemCount != null)
            {
                UpdateDelegateListItemCount(0);
            }

            try
            {
                seatNrNameTableLock.Enter();
                seatNrNameTable.Clear();
            }
            finally
            {
                seatNrNameTableLock.Leave();
            }

        }


        #endregion

        #region Microphones

        public void GetMicrophones()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.GetMicrophones();
        }

        public void GetMicrophoneStates()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.GetMicrophoneStates();
        }

        public void GetMicrophoneMode()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.GetMicrophoneMode();
        }

        public void ChangeMicrophoneMode(int tempMode)
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.ChangeMicrophoneMode(tempMode,maxSpeakers,maxRequests);
        }

        public void ChangeMicrophoneMaxActive(int tempMicActive)
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.ChangeMicrophoneMaxActive(micMode, tempMicActive, maxRequests);
        }

        public void ChangeMicrophoneMaxRequest(int tempMicRequest)
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.ChangeMicrophoneMaxRequest(micMode, maxSpeakers, tempMicRequest);
        }

        public void MicrophoneSpeakerListItemSelected(int index)
        {
            if(associatedTelevicModule != null)
            {   
                try
                {
                    microphoneStateLock.Enter();
                    if(microphoneState.speakers != null && microphoneState.speakers.Length != 0)
                        associatedTelevicModule.TurnOffMcirophone(microphoneState.speakers[index]);
                }
                finally
                {
                    microphoneStateLock.Leave();
                }
            }
        }

        public void MicrophoneRequestListItemSelected(int index)
        {
            if (associatedTelevicModule != null) 
            {
                try
                {
                    microphoneStateLock.Enter();
                    if(microphoneState.requests != null && microphoneState.requests.Length != 0)
                        associatedTelevicModule.TurnOffMcirophone(microphoneState.requests[index]);
                }
                finally
                {
                    microphoneStateLock.Leave();
                }
            }
        }

        internal void PopulateMicrophoneSummary(MicrophoneSummary temp)
        {
            if (temp != null)
            {
                if (temp.microphoneMode != null)
                {
                    PopulateMicrophoneMode(temp.microphoneMode);
                }

                if (temp.state != null)
                {
                    PopulateMicrophoneState(temp.state);
                }
            }
        }

        internal void PopulateMicrophoneMode(MicrophoneMode temp)
        {
            if (temp != null)
            {
                micMode = temp.mode;
                if (updateMicrophoneMode != null)
                    updateMicrophoneMode(temp.mode);

                maxSpeakers = temp.maxNrActive;
                if (updateMicrophoneMaxNrActive != null)
                    updateMicrophoneMaxNrActive(temp.maxNrActive);

                maxRequests = temp.maxNrRequest;
                if (updateMicrophoneMaxNrRequest != null)
                    updateMicrophoneMaxNrRequest(temp.maxNrRequest);
            }
        }
       

        internal void ClearMicrophoneState()
        {
            micMode = "";
            maxRequests = 0;
            maxSpeakers = 0;

            if (updateMicrophoneMode != null)
                updateMicrophoneMode("");
            if (updateMicrophoneMaxNrActive != null)
                updateMicrophoneMaxNrActive(0);
            if (updateMicrophoneMaxNrRequest != null)
                updateMicrophoneMaxNrRequest(0);

            try
            {
                microphoneStateLock.Enter();
                microphoneState = null;
            }
            finally
            {
                microphoneStateLock.Leave();
            }



            if (updateSpeakerListByIndex != null)
            {
                for (var i = 0; i < 10; i++)
                {
                    updateSpeakerListByIndex(i, "");
                }
            }

            if (updateSpeakerListItemCount != null)
                updateSpeakerListItemCount(0);


            if (updateRequestListByIndex != null)
            {
                for (var i = 0; i < 50; i++)
                {
                    updateRequestListByIndex(i, "");
                }
            }

            if (updateRequestListItemCount != null)
                updateRequestListItemCount(0);   
        }

        internal void UpdateMicrophoneState()
        {
            if (microphoneState != null)
            {
                try
                {
                    microphoneStateLock.Enter();
                    if (microphoneState.speakers != null)
                    {
                        if (updateSpeakerListByIndex != null)
                        {
                            try
                            {
                                seatNrNameTableLock.Enter();
                                for (var i = 0; i < microphoneState.speakers.Length; i++)
                                {

                                    string delegateName;
                                    //if (seatNrNameTable.ContainsKey(temp.speakers[i]))
                                    if (seatNrNameTable.TryGetValue(microphoneState.speakers[i], out delegateName))
                                    {
                                        //updateSpeakerListByIndex(i, seatNrNameTable[temp.speakers[i]]);
                                        updateSpeakerListByIndex(i, delegateName);
                                    }
                                    else
                                        updateSpeakerListByIndex(i, "Seat " + microphoneState.speakers[i]);

                                }
                            }
                            finally
                            {
                                seatNrNameTableLock.Leave();
                            }
                        }

                        if (updateSpeakerListItemCount != null)
                            updateSpeakerListItemCount(microphoneState.speakers.Length);
                    }
                    else
                    {
                        if (updateSpeakerListItemCount != null)
                            updateSpeakerListItemCount(0);
                    }

                    if (microphoneState.requests != null)
                    {
                        if (updateRequestListByIndex != null)
                        {
                            try
                            {
                                seatNrNameTableLock.Enter();
                                for (var i = 0; i < microphoneState.requests.Length; i++)
                                {

                                    string delegateName;
                                    //if (seatNrNameTable.ContainsKey(temp.speakers[i]))
                                    if (seatNrNameTable.TryGetValue(microphoneState.requests[i], out delegateName))
                                    {
                                        //updateSpeakerListByIndex(i, seatNrNameTable[temp.speakers[i]]);
                                        updateRequestListByIndex(i, delegateName);
                                    }
                                    else
                                        updateRequestListByIndex(i, "Seat " + microphoneState.requests[i]);

                                }
                            }
                            finally
                            {
                                seatNrNameTableLock.Leave();
                            }
                        }

                        if (updateRequestListItemCount != null)
                            updateRequestListItemCount(microphoneState.requests.Length);
                    }
                    else
                    {
                        if (updateRequestListItemCount != null)
                            updateRequestListItemCount(0);
                    }
                }
                finally
                {
                    microphoneStateLock.Leave();
                }
            }
        }

        internal void PopulateMicrophoneState(MicrophoneState temp)
        {
            microphoneState = temp;

            if(temp != null)
            {
                if (temp.speakers != null)
                {
                    if (updateSpeakerListByIndex != null)
                    {
                        try
                        {
                            seatNrNameTableLock.Enter();
                            for (var i = 0; i < temp.speakers.Length; i++)
                            {

                                string delegateName;
                                //if (seatNrNameTable.ContainsKey(temp.speakers[i]))
                                if (seatNrNameTable.TryGetValue(temp.speakers[i], out delegateName))
                                {
                                    //updateSpeakerListByIndex(i, seatNrNameTable[temp.speakers[i]]);
                                    updateSpeakerListByIndex(i, delegateName);
                                }
                                else
                                    updateSpeakerListByIndex(i, "Seat " + temp.speakers[i]);

                            }
                        }
                        finally
                        {
                            seatNrNameTableLock.Leave();
                        }
                    }

                    if (updateSpeakerListItemCount != null)
                        updateSpeakerListItemCount(temp.speakers.Length);
                }
                else
                {
                    if (updateSpeakerListItemCount != null)
                        updateSpeakerListItemCount(0);
                }

                if (temp.requests != null)
                {
                    if (updateRequestListByIndex != null)
                    {
                        try
                        {
                            seatNrNameTableLock.Enter();
                            for (var i = 0; i < temp.requests.Length; i++)
                            {

                                string delegateName;
                                //if (seatNrNameTable.ContainsKey(temp.speakers[i]))
                                if (seatNrNameTable.TryGetValue(temp.requests[i], out delegateName))
                                {
                                    //updateSpeakerListByIndex(i, seatNrNameTable[temp.speakers[i]]);
                                    updateRequestListByIndex(i, delegateName);
                                }
                                else
                                    updateRequestListByIndex(i, "Seat " + temp.requests[i]);

                            }
                        }
                        finally
                        {
                            seatNrNameTableLock.Leave();
                        }
                    }

                    if (updateRequestListItemCount != null)
                        updateRequestListItemCount(temp.requests.Length);
                }
                else
                {
                    if (updateRequestListItemCount != null)
                        updateRequestListItemCount(0);
                }
            }
        }

        #endregion

        #region Voting

        public void fnStartVoting()
        {
            if (associatedTelevicModule != null)
            {
                if (votingStatus == "Pause")
                    associatedTelevicModule.ResumeVoting();
                else if (votingStatus == "Start")
                    associatedTelevicModule.RestartVoting();
                else
                    associatedTelevicModule.StartVoting();
            }
        }

        public void fnPauseVoting()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.PauseVoting();
        }

        public void fnStopVoting()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.StopVoting();
        }

        public void fnClearVoting()
        {
            if (associatedTelevicModule != null)
                associatedTelevicModule.ClearVoting();
        }

        public void fnAddVoteWithTemplate(string temp)
        {
            if (associatedTelevicModule != null && temp != null)
                associatedTelevicModule.AddVoteWithTemplate(temp);
        }

        internal void TriggerVotingStateUpdate(VotingState temp)
        {
            if (updateVotingStatus != null && temp != null)
            {
                votingStatus = temp.state;
                updateVotingStatus(temp.state);
            }
        }

        internal void TriggerVotingOucomUpdate(VotingOutcomeUpdate temp)
        {
            if (updateVotingOutcome != null && temp != null)
                updateVotingOutcome(temp.outCome);
        }

        internal void TriggerVotingResultSummaryUpdate(VotingResultSummary temp)
        {
            if (temp == null) return;
            if (UpdateVotingTotal != null)
                UpdateVotingTotal(temp.votingResults.total.count, temp.votingResults.voted.weight.ToString(CultureInfo.InvariantCulture));
            if (UpdateVotingVoted != null)
                UpdateVotingVoted(temp.votingResults.voted.count, temp.votingResults.voted.weight.ToString(CultureInfo.InvariantCulture));
            if (UpdateVotingNonVoted != null)
                UpdateVotingNonVoted(temp.votingResults.notVoted.count, temp.votingResults.notVoted.weight.ToString(CultureInfo.InvariantCulture));
            if (UpdateVotingAuthrityPresent != null)
                UpdateVotingAuthrityPresent(temp.votingResults.authorityAssigned.present);
            if (UpdateVotingAuthorityVoted != null)
                UpdateVotingAuthorityVoted(temp.votingResults.authorityAssigned.voted);
            if (UpdateVotingAuthorityRegistered != null)
                UpdateVotingAuthorityRegistered(temp.votingResults.authorityAssigned.register);
        }

        internal void TriggerVotingTimerUpdate(BaseObjectTimer temp)
        {
            if (temp == null || temp.timer == null) return;
            if(updateVotingTimeUsed != null)
                updateVotingTimeUsed(temp.timer.timeUsed);

            if (updateVotingTotalTime != null)
                updateVotingTotalTime(temp.timer.totalTime);

            if (updateVotingWarningTime != null)
                updateVotingWarningTime(temp.timer.totalTime);
        }

        internal void TriggerVotingTimerStateUpdate(TimerState temp)
        {
            if (temp == null) return;
            if (updateVotingTimerPaused != null)
            {
                updateVotingTimerPaused(temp.paused ? 1 : 0);
            }

            TriggerVotingTimerUpdate(temp.votingTimer);
        }

        internal void ClearVotingData()
        {
            votingStatus = "Stop";
            if (updateVotingStatus != null)
                updateVotingStatus(votingStatus);

            if (updateVotingTimeUsed != null)
                updateVotingTimeUsed("");

            if (updateVotingTotalTime != null)
                updateVotingTotalTime("");

            if (updateVotingWarningTime != null)
                updateVotingWarningTime("");

            if (updateVotingTimerPaused != null)
                updateVotingTimerPaused(0);

            if (updateVotingOutcome != null)
                updateVotingOutcome("");

            if (UpdateVotingTotal != null)
                UpdateVotingTotal(0, "");
            if (UpdateVotingVoted != null)
                UpdateVotingVoted(0, "");
            if (UpdateVotingNonVoted != null)
                UpdateVotingNonVoted(0, "");
            if (UpdateVotingAuthrityPresent != null)
                UpdateVotingAuthrityPresent(0);
            if (UpdateVotingAuthorityVoted != null)
                UpdateVotingAuthorityVoted(0);
            if (UpdateVotingAuthorityRegistered != null)
                UpdateVotingAuthorityRegistered(0);

        }



        #endregion

    }
}